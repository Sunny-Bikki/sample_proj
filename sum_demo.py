#*** Sample code for demo ***
def add2(count_1:int, count_2:int)->int:
    #*** sum two numbers ***
    return count_1 + count_2

if __name=='__main__':
    print("sum of two counts:", add2(1,2))    
